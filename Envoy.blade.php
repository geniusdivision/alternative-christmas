@setup
    $project = "PROJECT_NAME";
    $urls = array(
        'live' => "LIVE_SITE_URL",
        'staging' => "STAGING_SITE_URL",
    );
@endsetup

@servers(['staging' => ['-A -p 2020 -l STAGING_ACCOUNT_USERNAME 109.108.143.91'],'live' => ['-A -p 2020 -l LIVE_ACCOUNT_USERNAME 109.108.143.91']])

@story('deploy')
    @if ($where == 'staging')
        deploy:staging
    @endif

    @if ($where == 'live')
        deploy:live
    @endif
@endstory

@task('deploy:staging', ['on' => 'staging'])
    cd REPO_NAME;
    sh git.sh;
    composer update;
@endtask

@task('deploy:live', ['on' => 'live'])
    cd REPO_NAME;
    sh git.sh;
    composer update;
@endtask

@error
    $user = exec('whoami');
    $payload='{
        "channel": "#deployments",
        "username": "Envoy Deployer",
        "text": "",
        "icon_emoji": ":rocket:",
        "attachments": [
            {
                "fallback": "Error with deployment",
                "color": "#BA003B",
                "title": "Error: Something went wrong with deployment from *'.$user.'*. Check your terminal.",
                "text": "",
            }
        ]
    }';
    $cmd = 'curl -X POST --data-urlencode \'payload='.$payload.'\' https://hooks.slack.com/services/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';
    exec($cmd);
    exit;
@enderror

@if ($slack)
    @finished
        $user = exec('whoami');
        $commit = exec('git log -1');
        $payload='{
            "channel": "#deployments",
            "username": "Envoy Deployer",
            "text": "",
            "icon_emoji": ":rocket:",
            "attachments": [
                {
                    "fallback": "'.$project.' successfully deployed to '. $urls[$where] . '",
                    "color": "#36a64f",
                    "title": "'.$project.' successfully deployed to '. $urls[$where] . '",
                    "title_link": "'.$urls[$where].'",
                    "text": "",
                    "fields": [
                        {
                            "title": "Last Commit:",
                            "value": "'.$commit.'",
                        }
                    ]
                }
            ]
        }';
        $cmd = 'curl -X POST --data-urlencode \'payload='.$payload.'\' https://hooks.slack.com/services/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';
        exec($cmd);
    @endfinished
@else
    @finished
        echo "Deployment complete. You deployed $project to $urls[$where] successfully.";
    @endfinished
@endif
