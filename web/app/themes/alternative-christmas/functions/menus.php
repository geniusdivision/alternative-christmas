<?php

function GDWP_Register_Menus() {
    register_nav_menus(
        array(
            'header-menu' => __( 'Header Menu', 'alternative-christmas' ),
            'footer-menu' => __( 'Footer Menu', 'alternative-christmas' ),
        )
    );
}
add_action( 'init', 'GDWP_Register_Menus' );
