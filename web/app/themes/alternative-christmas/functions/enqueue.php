<?php

function GDWP_Theme_Enqueue() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', '//code.jquery.com/jquery-1.12.4.min.js');
    wp_enqueue_script( 'jquery' );

    wp_enqueue_script( 'popper-js', '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js', ['jquery'], null, true );
    wp_enqueue_script( 'bootstrap-js', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js', ['jquery'], null, true );

    wp_enqueue_style( 'fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
    wp_enqueue_style( 'bootstrap-css', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css' );

    wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/assets/build/scripts.js', ['jquery'], null, true);
    wp_enqueue_style( 'theme-css', get_template_directory_uri() . '/assets/build/styles.css' );
}
add_action( 'wp_enqueue_scripts', 'GDWP_Theme_Enqueue' );
