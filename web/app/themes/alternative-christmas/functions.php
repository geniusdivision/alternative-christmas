<?php
// UNCOMMENT AS NEEDED
// include get_template_directory() . '/functions/acf.php';
include get_template_directory() . '/functions/enqueue.php';
// include get_template_directory() . '/functions/image-sizes.php';
include get_template_directory() . '/functions/menus.php';

function GDWP_Get_Image( $id, $size ) {
    if ( strpos( $id, '/uploads' ) !== false ) {
        $id = attachment_url_to_postid( $id );
    }
    $src = wp_get_attachment_image_src( $id, $size )[0];
    $srcset = wp_get_attachment_image_src( $id, $size.'-2x' )[0];
    $width = wp_get_attachment_image_src( $id, $size )[1];
    $height = wp_get_attachment_image_src( $id, $size )[2];
    $alt = get_post_meta( $id, '_wp_attachment_image_alt', true );

    return "<img src=\"$src\" srcset=\"$srcset 2x\" width=\"$width\" height=\"$height\" alt=\"$alt\" class=\"img-fluid\" />";
}

function GDWP_Get_File( $file ) {
    $arrContextOptions = array(
        "ssl" => array(
            "verify_peer"=>false,
            "verify_peer_name"=>false,
        ),
    );

    return file_get_contents( $file, false, stream_context_create($arrContextOptions) );
}
