<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!-- Fonts -->
        <!-- FONTS GO HERE -->
    <!-- /Fonts -->

    <?php if ( wp_site_icon() ) :
        echo wp_site_icon();
    else : ?>
        <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.png">
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.png">
        <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.svg" color="#ab1927">
        <meta name="theme-color" content="#ab1927">
        <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/assets/images/og-image.png" />
    <?php endif; ?>

    <title><?php wp_title(); ?></title>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header>
        <div class="container">
            <nav class="navbar navbar-expand-lg justify-content-between">
                <div class="navbar-brand logo">
                    <a href="<?php echo home_url(); ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" srcset="<?php echo get_template_directory_uri(); ?>/assets/images/logo@2x.png 2x" alt="Genius Division" width="38" height="75" />
                    </a>
                </div>

                <p class="mb-0">#altchristmaslist</p>

                <ul class="nav">
                    <?php wp_nav_menu( array(
                        'theme_location' => 'header-menu',
                        'container' => '',
                        'depth' => 2,
                        'items_wrap' => '%3$s',
                    ) ); ?>
                </ul>
            </nav>
        </div>
        <!-- /.container -->
    </header>
    <!-- /.site-header -->
