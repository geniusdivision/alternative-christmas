<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once(dirname(__FILE__) . '/vendor/autoload.php');
Env::init();

class GDEnvHelper {
	private $direction;
	private $what;

	public function __construct($direction,$what,$file)
	{
		$this->direction = $direction;
		$this->what = $what;
		$this->file = $file;
		$dotenv = new Dotenv\Dotenv(__DIR__);
		if (file_exists(dirname(__FILE__) . '/.env')) {
		    $dotenv->load();
		    $dotenv->required(['DB_NAME', 'DB_USER', 'DB_PASSWORD', 'WP_HOME', 'WP_SITEURL']);

		    $this->run();
		}
	}

	public function run()
	{
		if ($this->what == "db") {
			if ($this->direction == "pull") {
				exec("mysqldump --user=".escapeshellcmd(env("DB_USER"))." --password=" . escapeshellcmd(env("DB_PASSWORD")) ." --host=localhost --single-transaction --add-drop-table ".escapeshellcmd(env("DB_NAME"))." > ".$this->file);
			}
			else if ($this->direction == "push") {
				exec("mysql --user=".escapeshellcmd(env("DB_USER"))." --password=" . escapeshellcmd(env("DB_PASSWORD")) ." --host=localhost ".escapeshellcmd(env("DB_NAME"))." < ".$this->file);
			}
		}
	}
}

$helper = new GDEnvHelper($argv[1],$argv[2],$argv[3]);